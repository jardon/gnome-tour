# Chinese (China) translation for gnome-tour.
# Copyright (C) 2020 gnome-tour's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-tour package.
# Boyuan Yang <073plan@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-tour master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-tour/issues\n"
"POT-Creation-Date: 2021-02-19 09:23+0000\n"
"PO-Revision-Date: 2021-03-13 12:18-0500\n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: Chinese (China) <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: data/org.gnome.Tour.desktop.in.in:3 data/org.gnome.Tour.metainfo.xml.in.in:7
#: src/main.rs:19
msgid "Tour"
msgstr "导览"

#: data/org.gnome.Tour.desktop.in.in:4
msgid "Greeter & Tour"
msgstr "欢迎界面和导览"

#: data/org.gnome.Tour.desktop.in.in:9
msgid "Gnome;GTK;"
msgstr "Gnome;GTK;"

#: data/org.gnome.Tour.metainfo.xml.in.in:8
msgid "GNOME Tour and Greeter"
msgstr "GNOME 导览和欢迎界面"

#: data/org.gnome.Tour.metainfo.xml.in.in:10
msgid "A guided tour and greeter for GNOME."
msgstr "GNOME 的导览和欢迎界面程序。"

#: data/org.gnome.Tour.metainfo.xml.in.in:15
msgid "Main Window"
msgstr "主窗口"

#: data/org.gnome.Tour.metainfo.xml.in.in:38
msgid "The GNOME Project"
msgstr "GNOME 项目"

#: src/widgets/pages/welcome.rs:151
msgid "Start the Tour"
msgstr "开始导览"

#: src/widgets/pages/welcome.rs:161
msgid "Learn about new and essential features in {} {}."
msgstr "了解 {} {} 中的全新重要功能。"

#: src/widgets/paginator.rs:32
msgid "_Start"
msgstr "开始(_S)"

#: src/widgets/paginator.rs:33
msgid "_Next"
msgstr "前进(_N)"

#: src/widgets/paginator.rs:34 src/widgets/paginator.rs:35
msgid "_Close"
msgstr "关闭(_C)"

#: src/widgets/paginator.rs:36
msgid "_Previous"
msgstr "后退(_P)"

#: src/widgets/window.rs:51
msgid "Get an Overview"
msgstr "获得概览"

#: src/widgets/window.rs:52
msgid "Press Activities to see open windows and apps."
msgstr "点击“活动”按钮以查看窗口和应用。"

#: src/widgets/window.rs:61
msgid "Make Apps Your Own"
msgstr "让应用为你所用"

#: src/widgets/window.rs:62
msgid "Arrange the app grid to your liking."
msgstr "按照你的喜好排列应用网格。"

#: src/widgets/window.rs:71
msgid "Keep on Top with Workspaces"
msgstr "使用工作区掌控系统"

#: src/widgets/window.rs:72
msgid "Easily organise windows with the new workspaces view."
msgstr "使用新的工作区视图轻松管理窗口。"

#: src/widgets/window.rs:81
msgid "Up/Down for the Overview"
msgstr "向上/向下以查看概览"

#: src/widgets/window.rs:82
msgid "On a touchpad, use three-finger vertical swipes. Try it!"
msgstr "在触摸板上使用三指竖直滑动。试试吧！"

#: src/widgets/window.rs:91
msgid "Left/Right for Workspaces"
msgstr "向左/向右以查看工作区"

#: src/widgets/window.rs:92
msgid "On a touchpad, use three-finger horizontal swipes. Try it!"
msgstr "在触摸板上使用三指水平滑动。试试吧！"

#: src/widgets/window.rs:104
msgid "That's it! We hope that you enjoy {} {}."
msgstr "内容就是这些了！我们希望您能享受使用 {} {}。"

#: src/widgets/window.rs:107
msgid "To get more advice and tips, see the Help app."
msgstr "如需更多建议和提示，请参考“帮助”程序。"

#~ msgid "Welcome to {} {}"
#~ msgstr "欢迎来到 {} {}"

#~ msgid "Hi there! Take the tour to learn your way around and discover essential features."
#~ msgstr "你好！欢迎使用本导览程序来上手使用系统并掌握基本的使用方法。"

#~ msgid "_No Thanks"
#~ msgstr "不，谢谢(_N)"

#~ msgid "The activities view can also be used to switch windows and search."
#~ msgstr "活动视图也可用于切换窗口和执行搜索。"

#~ msgid "Just type to search"
#~ msgstr "直接键入文本以搜索"

#~ msgid "In the activities view, just start typing to search for apps, settings and more."
#~ msgstr "在活动视图中，只需直接开始打字便可搜索应用、设置和其它更多内容。"

#~ msgid "Click the time to see notifications"
#~ msgstr "点击时间以查看通知"

#~ msgid "The notifications popover also includes personal planning tools."
#~ msgstr "弹出的通知窗口也包含了个人计划工具。"

#~ msgid "View system information and settings"
#~ msgstr "查看系统信息和设置"

#~ msgid "Get an overview of the system status and quickly change settings."
#~ msgstr "获得系统状态的概览信息，也可快速修改设置。"

#~ msgid "Use Software to find and install apps"
#~ msgstr "使用“软件”程序查找并安装应用"

#~ msgid "Discover great apps through search, browsing and our recommendations."
#~ msgstr "使用浏览、搜索功能并查看我们的推荐项来发现精彩的应用程序。"
