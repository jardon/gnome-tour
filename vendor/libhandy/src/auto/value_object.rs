// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use glib;
use glib::object::IsA;
use glib::translate::*;
use glib::GString;
use handy_sys;
use std::fmt;

glib_wrapper! {
    pub struct ValueObject(Object<handy_sys::HdyValueObject, handy_sys::HdyValueObjectClass, ValueObjectClass>);

    match fn {
        get_type => || handy_sys::hdy_value_object_get_type(),
    }
}

impl ValueObject {
    pub fn new(value: &glib::Value) -> ValueObject {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(handy_sys::hdy_value_object_new(value.to_glib_none().0)) }
    }

    //pub fn new_collect(type_: glib::types::Type, : /*Unknown conversion*//*Unimplemented*/Fundamental: VarArgs) -> ValueObject {
    //    unsafe { TODO: call handy_sys:hdy_value_object_new_collect() }
    //}
}

pub const NONE_VALUE_OBJECT: Option<&ValueObject> = None;

pub trait ValueObjectExt: 'static {
    fn get_string(&self) -> Option<GString>;

    fn get_value(&self) -> Option<glib::Value>;
}

impl<O: IsA<ValueObject>> ValueObjectExt for O {
    fn get_string(&self) -> Option<GString> {
        unsafe {
            from_glib_none(handy_sys::hdy_value_object_get_string(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    fn get_value(&self) -> Option<glib::Value> {
        unsafe {
            from_glib_none(handy_sys::hdy_value_object_get_value(
                self.as_ref().to_glib_none().0,
            ))
        }
    }
}

impl fmt::Display for ValueObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ValueObject")
    }
}
